# inChurch Backend
A management system for users and departments

## Steps to use the API

- Clone this repository
- Open the repository folder on your machine
- Run `pip install -r requirements.txt`
- Run `python ./manage.py migrate`
- Run `python ./manage.py createsuperuser` and follow the instructions
- Please, check out the docs (https://app.swaggerhub.com/apis/Teiji/Inchurch-Backend/1.0) 
- Now you are able to login and use the API running the command `python .manage.py runserver`

## Observations and Instructions
- You shound run this API with Python 3
- Pay attention to the slash at the end of every endpoint
- The base URL (by default) is `http://localhost:8000/api` [(See more on docs)](https://app.swaggerhub.com/apis/Teiji/Inchurch-Backend/1.0)
- The Bearer token template is `Token <your_token>`

## About some of the decisions made
- I decided to use SQLite because it is easier and faster to start an application with it, since it is not necessary to do any initial setup to use it. Of course, in a real case where the API would go into production, the database would probably be PostgreSQL or MySQL

## Future Improvements
- Create a new level of permissions, something like a "Department Admin"
- More data like `Address` and `Contact` (Phone number, emails)

## Docs
- https://app.swaggerhub.com/apis/Teiji/Inchurch-Backend/1.0



Made with ❤️ by Teiji