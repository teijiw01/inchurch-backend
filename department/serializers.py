from rest_framework import serializers
from department.models import Department


class DepartmentSerializer(serializers.ModelSerializer):
    """Default department serializer"""

    class Meta:
        model = Department
        fields = ("id", "name", "created_at", "updated_at", "is_active")
        read_only_fields = ("id", "created_at", "updated_at", "is_active")
