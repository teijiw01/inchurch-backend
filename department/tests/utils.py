from django.forms.models import model_to_dict
from department.models import Department


def create_department(self, dept_obj={}):
    department = Department.objects.create(**self.department_default_obj, **dept_obj)
    return model_to_dict(department)
