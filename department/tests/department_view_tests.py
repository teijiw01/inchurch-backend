from rest_framework.test import APITestCase
from user_profile.models import UserProfile
from .utils import create_department


class DepartmentTestCase(APITestCase):
    def setUp(self):
        user = UserProfile.objects.create_superuser(
            email="john_doe@test.com",
            first_name="John",
            last_name="Doe",
            password="12345678",
        )
        self.client.force_authenticate(user=user)
        self.department_default_obj = {"name": "People"}

    def test_create_department(self):
        create_response = self.client.post(
            "/api/department/", self.department_default_obj
        )
        self.assertEqual(create_response.status_code, 201)

    def test_get_single_department(self):
        department = create_department(self)
        get_response = self.client.get(
            "/api/department/{}".format(department.get("id"))
        )
        self.assertEqual(get_response.status_code, 200)

    def test_get_single_not_existent_department(self):
        get_response = self.client.get("/api/department/{}".format(311))
        self.assertEqual(get_response.status_code, 404)

    def test_update(self):
        department = create_department(self)
        update_response = self.client.put(
            "/api/department/{}".format(department.get("id")),
            {"name": "IT"},
            format="json",
        )
        self.assertEqual(update_response.status_code, 200)

    def test_delete(self):
        department = create_department(self)
        delete_response = self.client.delete(
            "/api/department/{}".format(department.get("id"))
        )
        self.assertEqual(delete_response.status_code, 204)
