from rest_framework import permissions


class DepartmentCreateDeletePermission(permissions.BasePermission):
    def has_permission(self, request, view):
        if request.user.is_staff:
            return True
        if view.action in ["list", "destroy", "create"]:
            return False
        return True

    def has_object_permission(self, request, view, obj):
        if request.user.is_staff:
            return True
        if view.action in ["retrieve", "update"]:
            return obj == request.user.department
        return False
