from rest_framework import status
from rest_framework.response import Response
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.decorators import (
    action,
    authentication_classes,
    permission_classes,
)
from rest_framework.viewsets import ModelViewSet
from django.shortcuts import get_object_or_404

from department.serializers import DepartmentSerializer
from department.models import Department
from department.permissions import DepartmentCreateDeletePermission


class DepartmentViewSet(ModelViewSet):
    queryset = Department.objects.all()
    serializer_class = DepartmentSerializer
    permission_classes = (DepartmentCreateDeletePermission,)
    http_method_names = ["get", "post", "put", "delete"]

    def get_queryset(self):
        """Method to handle 'show_deleted' query param for 'list' action"""
        queryset = self.queryset
        if self.action == "list":
            query_params = self.request.query_params
            show_deleted = (
                str(query_params.get("show_deleted")).strip().lower() == "true"
            )
            if not show_deleted:
                queryset = queryset.filter(is_active=True)
        return queryset

    def destroy(self, request, pk):
        """Soft delete"""
        department = get_object_or_404(Department, pk=pk, is_active=True)
        department.is_active = False
        department.save()
        return Response(status=status.HTTP_204_NO_CONTENT)