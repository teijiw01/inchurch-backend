from django.urls import path, include
from rest_framework.routers import DefaultRouter
from department import views

router = DefaultRouter()
router.register("", views.DepartmentViewSet)

urlpatterns = [path("", include(router.urls))]
