from django.contrib import admin
from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from user_profile.views import register

urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/profile/", include("user_profile.urls")),
    path("api/department/", include("department.urls")),
    path("api/login/", obtain_auth_token),
    path("api/register/", register),
]
