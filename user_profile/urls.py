from django.urls import path
from user_profile import views

urlpatterns = [
    path("<int:pk>/", views.UserProfileUpdateDeleteView.as_view()),
    path("<int:pk>/admin/", views.set_admin_user),
    path("change_password/", views.change_own_password),
    path("", views.ListProfiles.as_view()),
    path("department/", views.ListMyDepartmentProfiles.as_view()),
]
