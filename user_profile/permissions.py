from rest_framework import permissions


class UserProfileUpdateDeletePermission(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user.is_staff:
            return True
        if request.method == "DELETE":
            return obj.id == request.user.id
        if request.method in ["PUT", "GET"]:
            return obj.department == request.user.department
        return False