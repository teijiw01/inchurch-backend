from django.db import models
from django.contrib.auth.models import (
    AbstractBaseUser,
    PermissionsMixin,
    BaseUserManager,
)
from django.shortcuts import get_object_or_404
from department.views import Department


class UserProfileManager(BaseUserManager):
    """Default manager for 'UserProfile' model"""

    def create_user(
        self,
        email,
        first_name,
        last_name,
        password=None,
        is_staff=False,
        is_superuser=False,
        department=None,
    ):

        if not email:
            raise ValueError("Users must have an email address")

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            is_staff=is_staff,
            is_superuser=is_superuser,
            department=department,
        )

        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_staffuser(self, email, first_name, last_name, password, department=None):
        user = self.create_user(
            email=email,
            first_name=first_name,
            last_name=last_name,
            password=password,
            is_staff=True,
            is_superuser=False,
            department=department,
        )
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password, department=None):
        user = self.create_user(
            email=email,
            first_name=first_name,
            last_name=last_name,
            password=password,
            is_staff=True,
            is_superuser=True,
            department=department,
        )
        user.save(using=self._db)
        return user


class DepartmentManager(models.Manager):
    """Manager to get profiles by department"""

    def get_queryset(self, department):
        return UserProfile.objects.all().filter(department=department)


class GetProfileManager(models.Manager):
    """Manager to get profile using default value for 'is_active' field"""

    def get_queryset(self, pk, only_active=True):
        args = {}
        if only_active:
            args = {"is_active": True}
        profile = get_object_or_404(UserProfile, pk=pk, **args)
        return profile


class UserProfile(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    updated_at = models.DateTimeField(auto_now=True)
    created_at = models.DateTimeField(auto_now_add=True)
    department = models.ForeignKey(
        Department, on_delete=models.CASCADE, blank=False, null=True
    )

    objects = UserProfileManager()
    departments = DepartmentManager().get_queryset
    get_profile_or_404 = GetProfileManager().get_queryset

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["first_name", "last_name"]

    def get_full_name(self):
        """Get full name of user profile"""
        return "{} {}".format(self.first_name, self.last_name)

    def __str__(self):
        return self.email
