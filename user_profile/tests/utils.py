from django.forms.models import model_to_dict
from user_profile.models import UserProfile


def set_admin_client(self):
    model = UserProfile.objects.create_staffuser(**self.admin_dict)
    self.client.force_authenticate(user=model)
    self.model = model
    return None


def set_non_admin_client(self):
    model = UserProfile.objects.create_user(**self.non_admin_dict)
    self.client.force_authenticate(user=model)
    self.model = model
    return None


def create_user(self, obj={}):
    default_email = "john_doe@usertest.com"
    default_password = "12345678"
    user = UserProfile.objects.create_user(
        **{
            **dict(
                email=default_email,
                first_name="John",
                last_name="Doe",
                password=default_password,
                department=self.people_department,
            ),
            **obj,
        }
    )
    return model_to_dict(user)
