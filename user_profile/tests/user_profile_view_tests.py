import json
from rest_framework.test import APITestCase
from department.models import Department
from .utils import set_admin_client, set_non_admin_client, create_user


class UserProfileTestCase(APITestCase):
    def setUp(self):
        self.people_department = Department.objects.create(name="People")
        self.it_department = Department.objects.create(name="IT")

        user_dict = dict(
            email="john_doe@test.com",
            first_name="John",
            last_name="Doe",
            password="12345678",
            department=self.people_department,
        )

        self.non_admin_dict = {**user_dict, "email": "john_doe@nonadmin.com"}
        self.admin_dict = {**user_dict, "email": "john_doe@admin.com"}

    def test_user_profile_register(self):
        profile_obj = {
            "email": "john_doe@defaulttest.com",
            "first_name": "John",
            "last_name": "Doe",
            "password": "12345678",
            "department": self.people_department.id,
        }
        response = self.client.post(
            "/api/register/",
            profile_obj,
            format="json",
        )
        self.assertEqual(response.status_code, 201)

    def test_user_profile_register_without_required_fields(self):
        profile_obj = {
            "email": None,
            "first_name": None,
            "last_name": "Doe",
            "password": "12345678",
            "department": self.people_department.id,
        }
        response = self.client.post(
            "/api/register/",
            profile_obj,
            format="json",
        )
        self.assertEqual(response.status_code, 400)

    def test_admin_get_single_profile(self):
        """Testing an admin permission to get a single profile"""
        set_admin_client(self)
        user = create_user(self)
        response = self.client.get("/api/profile/{}".format(user.get("id")))
        self.assertEqual(response.status_code, 200)

    def test_non_admin_same_department_get_single_profile(self):
        """Testing a non-administrator's permission to get a single profile from the same department as him"""
        set_non_admin_client(self)
        user = create_user(self, {"department": self.model.department})
        response = self.client.get("/api/profile/{}".format(user.get("id")))
        self.assertEqual(response.status_code, 200)

    def test_non_admin_different_department_get_single_profile(self):
        """Testing a non-administrator's permission to get a single profile from a different department"""
        set_non_admin_client(self)
        profile_obj = {
            "first_name": "foo",
            "last_name": "bar",
            "password": "different_dept",
            "email": "foo_bar_test@nonadmin.com",
            "department": self.it_department,
        }
        user = create_user(self, profile_obj)
        response = self.client.get("/api/profile/{}".format(user.get("id")))
        self.assertEqual(response.status_code, 403)

    def test_get_non_existent_user_profile(self):
        set_non_admin_client(self)
        response = self.client.get("/api/profile/{}".format(311))
        self.assertEqual(response.status_code, 404)

    def test_update(self):
        set_admin_client(self)
        user = create_user(self)
        updated_object = {
            **user,
            "email": "foo_bar@test.com",
            "first_name": "foo",
            "last_name": "bar",
        }
        response = self.client.put(
            "/api/profile/{}".format(user.get("id")),
            updated_object,
            format="json",
        )
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data.get("id"), updated_object["id"])
        self.assertEqual(response.data.get("first_name"), updated_object["first_name"])
        self.assertEqual(response.data.get("last_name"), updated_object["last_name"])

    def test_non_admin_same_department_update(self):
        set_non_admin_client(self)
        user = create_user(self)
        updated_object = {
            **user,
            "email": "foo_bar@test.com",
            "first_name": "foo",
            "last_name": "bar",
        }
        response = self.client.put(
            "/api/profile/{}".format(user.get("id")),
            updated_object,
            format="json",
        )
        self.assertEqual(response.status_code, 200)

    def test_non_admin_different_department_update(self):
        set_non_admin_client(self)
        profile_obj = {
            "first_name": "foo",
            "last_name": "bar",
            "password": "different_dept",
            "email": "foo_bar_test@nonadmin.com",
            "department": self.it_department,
        }
        user = create_user(self, profile_obj)
        updated_object = {
            **user,
            "email": "foo_bar@test.com",
            "first_name": "foo",
            "last_name": "bar",
        }
        update_response = self.client.put(
            "/api/profile/{}".format(user.get("id")),
            updated_object,
            format="json",
        )
        self.assertEqual(update_response.status_code, 403)

    def test_non_admin_delete(self):
        set_non_admin_client(self)
        user = create_user(self)
        delete_response = self.client.delete("/api/profile/{}".format(user.get("id")))
        self.assertEqual(delete_response.status_code, 403)

    def test_admin_delete(self):
        set_admin_client(self)
        user = create_user(self)
        delete_response = self.client.delete("/api/profile/{}".format(user.get("id")))
        self.assertEqual(delete_response.status_code, 204)

    def test_self_delete(self):
        set_non_admin_client(self)
        response = self.client.delete("/api/profile/{}".format(self.model.id))
        self.assertEqual(response.status_code, 204)

    def test_non_admin_set_an_user_as_admin(self):
        set_non_admin_client(self)
        user = create_user(self)
        set_admin_response = self.client.patch(
            "/api/profile/{}/admin".format(user.get("id"))
        )
        self.assertEqual(set_admin_response.status_code, 403)

    def test_dmin_set_an_user_as_admin(self):
        set_admin_client(self)
        user = create_user(self)
        set_admin_response = self.client.patch(
            "/api/profile/{}/admin".format(user.get("id"))
        )
        self.assertEqual(set_admin_response.status_code, 204)
