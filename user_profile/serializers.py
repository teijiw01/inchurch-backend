from rest_framework import serializers
from user_profile.models import UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    """Serializer for profile details"""

    class Meta:
        model = UserProfile
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "is_active",
            "is_staff",
            "updated_at",
            "created_at",
            "department",
        )
        read_only_fields = ["id", "is_active", "is_staff", "updated_at", "created_at"]


class ListProfilesSerializer(serializers.ModelSerializer):
    """Serializer for profiles listing"""

    full_name = serializers.CharField(source="get_full_name")

    class Meta:
        model = UserProfile
        fields = ("id", "full_name")
        read_only_fields = ["full_name"]


class RegisterSerializer(serializers.ModelSerializer):
    """Serializer for user registration"""

    class Meta:
        model = UserProfile
        extra_kwargs = {
            "password": {"write_only": True},
            "department": {"required": False},
        }
        fields = (
            "id",
            "email",
            "first_name",
            "last_name",
            "password",
            "department",
        )

    def create(self, validated_data):
        user = UserProfile(
            email=validated_data["email"],
            first_name=validated_data["first_name"],
            last_name=validated_data["last_name"],
            department=validated_data.get("department", None),
        )

        user.set_password(validated_data["password"])

        user.save()

        return user


class ChangePasswordSerializer(serializers.Serializer):
    """Serializer for an user change own password"""

    model = UserProfile
    new_password = serializers.CharField(required=True)
    old_password = serializers.CharField(required=True)