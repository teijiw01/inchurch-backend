from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)
from rest_framework.generics import ListAPIView
from rest_framework import serializers
from django.http import HttpResponseBadRequest
from user_profile.permissions import UserProfileUpdateDeletePermission
from user_profile.serializers import (
    UserProfileSerializer,
    ListProfilesSerializer,
    RegisterSerializer,
    ChangePasswordSerializer,
)
from user_profile.models import UserProfile


@api_view(["POST"])
@permission_classes([AllowAny])
@authentication_classes([])
def register(request):
    """User register"""
    serializer = RegisterSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["PATCH"])
@permission_classes([IsAdminUser])
def set_admin_user(request, pk):
    """Set an user as admin"""
    profile = UserProfile.get_profile_or_404(pk=pk)
    profile.is_staff = True
    profile.save()
    return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(["PATCH"])
def change_own_password(request):
    """User change own password"""

    profile = UserProfile.get_profile_or_404(pk=request.user.id)
    serializer = ChangePasswordSerializer(data=request.data)
    if serializer.is_valid():
        if not profile.check_password(serializer.data.get("old_password")):
            return Response(
                {"old_password": "Wrong password"}, status=status.HTTP_400_BAD_REQUEST
            )
        profile.set_password(serializer.data.get("new_password"))
        profile.save()
        return Response(status=status.HTTP_204_NO_CONTENT)
    return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ListMyDepartmentProfiles(ListAPIView):
    serializer_class = ListProfilesSerializer

    def get_queryset(self):
        department = self.request.user.department
        profiles = UserProfile.departments(department)
        return profiles


class ListProfiles(ListAPIView):
    serializer_class = ListProfilesSerializer
    permission_classes = (IsAdminUser,)

    def get_queryset(self):
        query_params = self.request.query_params
        dept_id = query_params.get("dept_id", None)
        if dept_id:
            try:
                dept_id = int(dept_id)
            except ValueError:
                raise serializers.ValidationError(
                    {"detail": "'dept_id' should be integer"}
                )
            profiles = UserProfile.departments(dept_id)
        else:
            profiles = UserProfile.objects.all()
        show_deleted = str(query_params.get("show_deleted")).strip().lower() == "true"
        if not show_deleted:
            profiles = profiles.filter(is_active=True)
        return profiles


class UserProfileUpdateDeleteView(APIView):
    permission_classes = (UserProfileUpdateDeletePermission,)

    def get(self, request, pk):
        profile = UserProfile.get_profile_or_404(pk=pk, only_active=False)
        self.check_object_permissions(self.request, profile)
        serializer = UserProfileSerializer(profile, many=False)
        return Response(serializer.data)

    def delete(self, request, pk):
        """Soft delete"""
        profile = UserProfile.get_profile_or_404(pk=pk)
        self.check_object_permissions(self.request, profile)
        profile.is_active = False
        profile.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, pk):
        profile = UserProfile.get_profile_or_404(pk=pk)
        self.check_object_permissions(self.request, profile)
        serializer = UserProfileSerializer(profile, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
